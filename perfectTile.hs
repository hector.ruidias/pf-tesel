{-
Teselas cuadradas
T= [A,B,C,D]

A= blanco/gris. Blanco en arista superior e izquierda y Gris en arista inferior y derecha

B= blanco/turquesa. Blanco en arista inferior y derecha. Turquesa en arista superior e izquierda.

C= gris/celeste.Gris en arista superior y derecha.Celeste arista inferior e izquierda.

D= turquesa/celeste: Celeste en arista superior derecha y turquesa en arista inferior izquierda.

O= Orientación de la Tesela [0,1,2,3]

Una tesela es una tupla (x o), donde x ∈ T

-}

import Data.Bits (Bits(xor))
import Data.List
import Debug.Trace

-- esto hay que quitar, al final harcodee las teselas en la lista de entrada y objetivo
tesels=["A","B","C","D"]
orientation=[0,1,2,3]
-- moves=[sftL,sftR,sftU,sft]
rotateTesel=[cwr,cwr']

--escenario inicial(default)
tableroInicial=[
                ("A",0),("A",2),("A",3),("A",1),
                ("C",0),("A",1),("A",0),("C",3),
                ("D",0),("C",0),("C",3),("D",3),
                ("B",0),("D",0),("D",3),("B",3),
                ("A",3),("B",1),("B",3),("A",2)]

--escenario final (default)
tableroFinal=[
                ("A",3),("A",3),("A",3),("A",3),
                ("B",3),("B",3),("B",3),("B",3),
                ("D",2),("D",2),("D",2),("D",2),
                ("C",0),("C",0),("C",0),("C",0),
                ("A",3),("A",3),("A",3),("A",3)]

--distancia de manhattan para cada tipo de ficha...por ahora será a mano, más adelante deberá calcularse
fichaA=[0,0,0,0,
        1,1,1,1,
        2,2,2,2,
        1,1,1,1,
        0,0,0,0]

fichaB=[1,1,1,1,
        0,0,0,0,
        1,1,1,1,
        2,2,2,2,
        3,3,3,3]

fichaC=[3,3,3,3,
        2,2,2,2,
        1,1,1,1,
        0,0,0,0,
        1,1,1,1]

fichaD=[2,2,2,2,
        1,1,1,1,
        0,0,0,0,
        2,2,2,2,
        2,2,2,2]

--lista de coordenadas
listaCoord=[(a,b)|a<-[1..5],b<-[1..4]]

--giro horario de la tesela
cwr :: (Ord b, Num b) => (a, b) -> (a, b)
cwr t   | snd t < 3 = (fst t , snd t + 1)
        | otherwise = (fst t, 0)
--giro antihorario de la tesela
cwr' :: (Ord b, Num b) => (a, b) -> (a, b)
cwr' t  | snd t > 0 = (fst t, snd t -1)
        | otherwise = (fst t, 3)

--desplazamos en direccion U,R,D,L 

--función de costo 1: lugar de tesela no ocupado por una tesela de su tipo
costfnPos [] [] = 0
costfnPos (a:az) (b:bz) | fst a /= fst b = 1 + costfnPos az bz
                        | otherwise = costfnPos az bz

--funcion de costo 2: se pondera para las fichas

costFicha [] [] [] x = 0
costFicha (a:az) (b:bz) (c:cz)  x       | fst a /= fst b && fst a == x = c + costFicha az bz cz x
                                        | otherwise = costFicha az bz cz x

--funcion de costo total: se suman todas la funciones de costo
costoTotal t0 t1 = costfnPos t0 t1 + costFicha t0 t1 fichaA "A" + costFicha t0 t1 fichaB "B" + costFicha t0 t1 fichaC "C" + costFicha t0 t1 fichaD "D"

--funcion de orientacion de teselas
orientarTesela tsO tsT  | snd tsO == snd tsT = ([],tsO)
                        | snd tsO == 3 && snd tsT == 0 = (["cwr"] ,cwr tsO)
                        | snd tsO == 0 && snd tsT == 3 = (["cwr'"],cwr' tsO)
                        | snd tsO - snd tsT > 0 = (["cwr'"] ++ fst (orientarTesela (cwr' tsO) tsT), snd (orientarTesela (cwr' tsO) tsT))
                        | otherwise  = (["cwr"] ++ fst (orientarTesela (cwr tsO) tsT), snd (orientarTesela (cwr tsO) tsT))


--- conversor (x,y) a indice de lista
accessxy coord dim list| fst coord > fst dim || snd coord > snd dim || fst coord < 1 || snd coord < 1 = error "out" --da una excepcion si  sale de los confines del tablero
                        | otherwise = idx
                                        where
                                                idx = (fst coord - 1)* snd dim + snd coord -1

reemplazar coord byTes list = resultList  --reemplazamos una tesela en la posicion (x,y) del tablero, ej: 
-- para probar
-- >>> reemplazar (1,3) ("X",3) tableroInicial
                                where
                                        pos= accessxy coord (5,4) list --traducimos a posicion en la lista
                                        resultList = take pos list ++ [byTes] ++ tail (drop pos list) -- para reemplazar en la posicion


--funcion que determina la adyascencia de dos teselas  ---desactivado por ahora (evaluar todo a true)
adyascentes coord1 coord2 | abs (fst coord1 - fst coord2) > 1  = True
                          | otherwise  = True



--intercambia dos teselas adyascentes al estilo "Candy Crush"
intercambiar coord1 coord2 list |not (adyascentes coord1 coord2 )= error "no son adyascentes"
                                | otherwise  = resultList
                                        where
                                                tes1 = list !! accessxy coord1 (5,4) list
                                                tes2 = list !! accessxy coord2 (5,4) list
                                                resultList= reemplazar coord2 tes1 (reemplazar coord1 tes2 list)



--ETAPA 1: cambiando las fichas de ubicación: estrategia CANDY CRUSH

--marcamos todas las fichas fuera de lugar recorremos las fichas
---- desubicadas genera la lista de pares ordenados para las fichas que no están en su posición objetivo, el primer argumento y segundo son los tableros a comparar y el tercero es una lista de coordenadas
desubicadas [] [] []= []
desubicadas (a:at) (b:bt) (c:ct)| fst a == fst b = desubicadas at bt ct
                                | otherwise = c: desubicadas at bt ct

-- funcion para generar pares intercambiables, para eso tenemos que definir un función que toma los pares ordenados de las piezas que se encuentran fuera de lugar
generarIntercambiables lst = filter (\x -> adyascentes (fst x) (snd x)) (nubBy (\x y -> x == (snd y, fst y)) [ (a, b)| a<-lst, b<-lst])


-- -- recorremos la lista de intercambiables realizando los intercambios, ponderando y ordenando las hojas para aplicar recursivamente

resolverlight tbl0 tbl1 | costoTotal tbl0 tbl1 == 0 = tbl0
                        | otherwise = trace ("\nMovimiento aplicado: " ++ show mov ++  "\nCosto : " ++ show costo ++ "\nTablero: " ++ show tbl_result ++ "\n=========>>>>>>>>" ++ show lstIntercambiables) $ resolverlight tbl_result tbl1
                        where
                                lstIntercambiables =  generarIntercambiables (desubicadas tbl0 tbl1 listaCoord)
                                tablerosGenerados =[ (t, intercambiar (fst t) (snd t) tbl0) | t <- lstIntercambiables]
                                costosTableros = [ costoTotal (snd x) tbl1 | x<-tablerosGenerados]
                                tablerosOrdenados= sortOn (\c -> fst c) (zip costosTableros tablerosGenerados)
                                tbl_result = snd (snd (head tablerosOrdenados))
                                costo = fst (head tablerosOrdenados)
                                mov=fst (snd (head tablerosOrdenados))
--trace ("movimiento aplicado: " ++ show mov ++ "\nTablero: " ++ show tbl_result ++ "\nCosto : " ++ show costo) $ 
--ETAPA 2: rotación de fichas
--aplicamos un zip de la funcion orientar tesela entre el tablero resultante de la etapa anterio y la objetivo
--para probar, escriban reorientadas nomás, luego pueden contrastar como cambiaron las teselas con print tableroInicial

reorientadas tblOrdenado  = zipWith orientarTesela tblOrdenado tableroFinal --tableroInicial puse para probar nomás, cuando este el tablero resultante de la etapa 1, reemplazo


--programa main invocando a todas las funciones por etapa
main= do
        let tbl = resolverlight tableroInicial tableroFinal
        print "=========TESELAS PERFECTAS =========="
        print "Etapa 1: Movemos las teselas"
        print tbl
        print "Etapa 2: orientamos las teselas correctamente"
        print ""
        print (reorientadas  tbl )
